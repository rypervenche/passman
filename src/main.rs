// TODO: Wait for command to finish instead of sleeping
use anyhow::Result;
use clap::Parser;
use clipboard::{ClipboardContext, ClipboardProvider};
use gpgme::{Context, Protocol};
use image::Luma;
use itertools::Itertools;
use once_cell::sync::Lazy;
use qrcode::{self, QrCode};
use regex::Regex;
use std::collections::HashMap;
use std::fmt;
use std::fs::File;
use std::io::Write;
use std::process::{Command, Stdio};
use std::time::Duration;
use std::{env, thread};
use thiserror::Error;

const USERNAME_SLEEP: u64 = 1;
const PASSWORD_SLEEP: u64 = 3;
const QRCODE_IMAGE_PATH: &str = "/tmp/qr.png";
const QRCODE_IMAGE_PATH2: &str = "/tmp/qr2.png";

static TRANSLATIONS: Lazy<HashMap<&str, &str>> = Lazy::new(|| {
    let mut m = HashMap::new();
    m.insert("en_US", "Select site");
    m.insert("es_MX", "Elegir sitio web");
    m.insert("fi_FI", "Valitse sivusto");
    m.insert("fr_FR", "Choisir site web");
    m.insert("zh_TW", "選擇綱站");
    m
});

#[derive(Error, Debug)]
pub enum CustomError {
    #[error("Can't find requested site")]
    SiteNotFound,
    #[error("No input provided")]
    NoInput,
    #[error("Input did not match regex")]
    BadInput,
    #[error("PASSMAN_DB env variable must be exported")]
    DBNotFound,
}

#[derive(Debug, Clone, PartialEq)]
struct Account {
    site: String,
    username: String,
    password: String,
    comment: Option<String>,
}

/// CLI options
#[derive(Parser, Debug)]
#[clap(version)]
#[clap(about = "Password manager with clipboard and QR code support.")]
struct Opts {
    /// Output credentials to command line
    cli: Option<String>,

    /// Output to QR code instead of clipboard
    #[clap(short, long)]
    qr_code: bool,
}

impl fmt::Display for Account {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "\n{}\n{}\n{}", self.site, self.username, self.password)
    }
}

impl Account {
    fn copy_to_clipboard(&self) -> Result<()> {
        let mut clip: ClipboardContext =
            ClipboardProvider::new().expect("Failed to create clipboard provider.");
        clip.set_contents(self.username.clone())
            .expect("Failed to set username.");
        thread::sleep(Duration::from_secs(USERNAME_SLEEP));
        clip.set_contents(self.password.clone())
            .expect("Failed to set password.");
        thread::sleep(Duration::from_secs(PASSWORD_SLEEP));
        Ok(())
    }
}

fn choice_phrase() -> &'static str {
    if let Ok(s) = env::var("LANG") {
        let (locale, _) = s.split('.').collect_tuple().unwrap();
        match TRANSLATIONS.get(locale) {
            Some(phrase) => phrase,
            None => TRANSLATIONS["en_US"],
        }
    } else {
        TRANSLATIONS["en_US"]
    }
}

fn unwrap_regex(capture: &regex::Captures, string: &str) -> Result<String> {
    capture
        .name(string)
        .map(|m| m.as_str().to_string())
        .ok_or_else(|| CustomError::BadInput.into())
}

fn parse_db(contents: &str) -> Result<Vec<Account>> {
    let mut creds: Vec<Account> = Vec::new();
    let re = Regex::new(
        r"(?x)
        (?P<site>[^\x00]*)     # Site
        \x00
        (?P<username>[^\x00]*) # Username
        \x00
        (?P<password>[^\x00]*) # Password
        \x00
        (?P<comment>[^\x00]*)  # Comment
        ",
    )
    .unwrap();

    for line in contents.lines() {
        let capture = re.captures(line).ok_or(CustomError::BadInput)?;

        let cred = Account {
            site: unwrap_regex(&capture, "site")?,
            username: unwrap_regex(&capture, "username")?,
            password: unwrap_regex(&capture, "password")?,
            comment: capture
                .name("comment")
                .filter(|m| m.as_str() != "")
                .map(|m| m.as_str().to_string()),
        };
        creds.push(cred);
    }
    Ok(creds)
}

fn find_accounts(vec: &[Account], query: &str) -> Vec<Account> {
    vec.iter()
        .filter(|cred| cred.site.to_lowercase().contains(query))
        .cloned()
        .collect::<Vec<_>>()
}

fn dmenu(vec: &[Account]) -> Result<Account> {
    let max_len = &vec
        .iter()
        .max_by(|cred1, cred2| cred1.site.len().cmp(&cred2.site.len()))
        .unwrap()
        .site
        .len();

    let sites: String = vec
        .iter()
        .map(|cred| {
            if cred.comment.is_some() {
                let site_len = cred.site.len();
                format!(
                    "{}{}{}",
                    cred.site.clone(),
                    " ".repeat(max_len - site_len + 2),
                    cred.comment.as_ref().unwrap()
                )
            } else {
                cred.site.clone()
            }
        })
        .collect::<Vec<String>>()
        .join("\n");

    let mut child = Command::new("dmenu")
        .args(["-i", "-l", "10", "-p", choice_phrase()])
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .spawn()?;

    {
        let stdin = child.stdin.as_mut().expect("Failed to open stdin");
        stdin
            .write_all(sites.as_bytes())
            .expect("Failed to write to stdin");
    }

    let output = child.wait_with_output().expect("Failed to read stdout");

    let choice: String = String::from_utf8_lossy(&output.stdout)
        .trim()
        .to_string()
        .to_lowercase();

    if choice.is_empty() {
        return Err(CustomError::NoInput.into());
    }

    // starts_with in opposite order, since choice has comment attached
    vec.iter()
        .filter(|cred| choice.starts_with(&cred.site.to_lowercase()))
        .max_by(|x, y| x.site.cmp(&y.site))
        .cloned()
        .ok_or_else(|| CustomError::SiteNotFound.into())
}

fn gpg_decrypt(file: &str) -> Result<String> {
    let mut ctx = Context::from_protocol(Protocol::OpenPgp)?;
    let mut input = File::open(file)?;
    let mut output = Vec::new();
    ctx.decrypt(&mut input, &mut output)?;

    let decrypted_db = String::from_utf8(output)?;
    Ok(decrypted_db)
}

fn to_image(site: &Account) -> Result<()> {
    let code = QrCode::new(&site.password).expect("Can't create QR code.");
    let image = code.render::<Luma<u8>>().build();
    image.save(QRCODE_IMAGE_PATH)?;
    Command::new("magick")
        .args([
            QRCODE_IMAGE_PATH,
            "-background",
            "White",
            "-pointsize",
            "50",
            &format!("label:{}", site.username),
            "-gravity",
            "Center",
            "-append",
            QRCODE_IMAGE_PATH2,
        ])
        .status()?;
    std::fs::rename(QRCODE_IMAGE_PATH2, QRCODE_IMAGE_PATH)?;
    Ok(())
}

fn display_image() -> Result<()> {
    Command::new("sxiv")
        .args(["-N", "passman", QRCODE_IMAGE_PATH])
        .status()?;
    Ok(())
}

fn main() -> Result<()> {
    let opts = Opts::parse();

    let encrypted_db: String = env::var("PASSMAN_DB").or(Err(CustomError::DBNotFound))?;
    let decrypted_db: String = gpg_decrypt(&encrypted_db)?;

    let results: Vec<Account> = parse_db(&decrypted_db)?;
    if let Some(site) = opts.cli {
        let accounts = find_accounts(&results, &site);
        for account in accounts {
            println!("{}", account);
        }
    } else {
        let account: Account = dmenu(&results)?;

        if opts.qr_code {
            to_image(&account)?;
            display_image()?;
            std::fs::remove_file(QRCODE_IMAGE_PATH)?;
        } else {
            account.copy_to_clipboard()?;
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const FAKE_DB: &str = "\
Google.com user1 password123 Gmail
Yahoo.com user2 password456 
Hotmail.com user3 password789 
";

    #[test]
    fn regex_check() {
        let correct_vec: Vec<Account> = vec![
            Account {
                site: "Google.com".to_string(),
                username: "user1".to_string(),
                password: "password123".to_string(),
                comment: Some("Gmail".to_string()),
            },
            Account {
                site: "Yahoo.com".to_string(),
                username: "user2".to_string(),
                password: "password456".to_string(),
                comment: None,
            },
            Account {
                site: "Hotmail.com".to_string(),
                username: "user3".to_string(),
                password: "password789".to_string(),
                comment: None,
            },
        ];

        assert_eq!(correct_vec, parse_db(FAKE_DB).unwrap());
    }
}
